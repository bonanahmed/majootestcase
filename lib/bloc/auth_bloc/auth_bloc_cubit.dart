import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_storage/get_storage.dart';
import 'package:majootestcase/models/user.dart';
import '../../utils/database_helper.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());
  // reference to our single class that manages the database
  final dbHelper = DatabaseHelper.instance;

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    GetStorage getStorage = GetStorage();
    bool isLoggedIn = getStorage.read("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void registerUser(User user) async {
    dbHelper.insert({
      "username": user.username,
      "email": user.email,
      "password": user.password
    }).then((value) {
      {
        Fluttertoast.showToast(msg: "Berhasil Daftar");
        emit(AuthBlocLoginState());
      }
    });
  }

  void loginUser(User user) {
    GetStorage getStorage = GetStorage();
    dbHelper.login(user.email, user.password).then((value) {
      if (value.isEmpty) {
        Fluttertoast.showToast(msg: "Email atau password salah");
      } else {
        getStorage.write("is_logged_in", true);
        Fluttertoast.showToast(msg: "Berhasil Login");
        String data = user.toMap().toString();
        getStorage.write("user_value", data);
        emit(AuthBlocLoggedInState());
      }
    });
  }

  void toRegister() async {
    emit(AuthBlocRegisterState());
  }

  void toLogin() async {
    emit(AuthBlocLoginState());
  }
}
