class User {
  // int _id;
  String email;
  String username;
  String password;

  User({this.email, this.username, this.password});

  User.fromMap(dynamic obj) {
    this.username = obj['username'];
    this.password = obj['password'];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = username;
    map["password"] = password;
    return map;
  }
}
