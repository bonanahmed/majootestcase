import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data> data;
  final Function goToDetail;
  final Function logOut;
  const HomeBlocLoadedScreen({Key key, this.data, this.logOut, this.goToDetail})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Daftar Movie"),
        actions: [
          TextButton(
            onPressed: logOut,
            child: Text(
              "Logout",
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return movieItemWidget(data[index]);
        },
      ),
    );
  }

  Widget movieItemWidget(Data data) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0))),
      child: InkWell(
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
        onTap: () {
          goToDetail(data);
        },
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(25),
              child: Image.network(data.i.imageUrl),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: Text(data.l, textDirection: TextDirection.ltr),
            )
          ],
        ),
      ),
    );
  }
}
