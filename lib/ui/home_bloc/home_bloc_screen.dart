import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/ui/home_bloc/home_detail_movie_bloc.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  final Function logOut;
  const HomeBlocScreen({Key key, this.logOut}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
      if (state is HomeBlocLoadedState) {
        return HomeBlocLoadedScreen(
          logOut: logOut,
          data: state.data,
          goToDetail: (data) {
            context.bloc<HomeBlocCubit>().goToDetail(data);
          },
        );
      } else if (state is HomeBlocLoadingState) {
        return LoadingIndicator();
      } else if (state is HomeBlocInitialState) {
        return LoadingIndicator();
      } else if (state is HomeBlocDetailState) {
        return HomeDetailMoviePage(
          data: state.data,
          goBack: () {
            context.bloc<HomeBlocCubit>().fetchingData();
          },
        );
      } else if (state is HomeBlocErrorState) {
        return ErrorScreen(
          message: state.error,
          retryButton: IconButton(
            onPressed: () {
              context.bloc<HomeBlocCubit>().fetchingData();
            },
            icon: Icon(
              Icons.refresh_sharp,
              color: Colors.black,
            ),
          ),
        );
      }

      return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""));
    });
  }
}
