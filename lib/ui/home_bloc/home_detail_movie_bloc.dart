import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';

class HomeDetailMoviePage extends StatelessWidget {
  final Data data;
  final Function goBack;
  const HomeDetailMoviePage({Key key, this.data, this.goBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return goBack();
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Detail Movie"),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              goBack();
            },
          ),
        ),
        body: ListView(
          padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 15),
              height: 250,
              width: MediaQuery.of(context).size.width,
              child: Image(
                image: NetworkImage("${data.i.imageUrl}"),
                fit: BoxFit.contain,
              ),
            ),
            Text(
              "${data.l}",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            Text(
              "${data.year}",
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
            ),
            Container(
              margin: EdgeInsets.only(top: 25, bottom: 15),
              child: Text(
                "Series",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
            (data.series != null)
                ? SizedBox(
                    height: 235,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: data.series.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.symmetric(horizontal: 15),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(bottom: 15),
                                height: 150,
                                child: Image(
                                  image: NetworkImage(
                                      "${data.series[index].i.imageUrl}"),
                                ),
                              ),
                              Container(
                                width: 130,
                                child: Text(
                                  "${data.series[index].l}",
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  )
                : Container(
                    child: Text("Tidak ada series"),
                  )
          ],
        ),
      ),
    );
  }
}
